%factos para testes
horario('3', [63000, 63480, 63780, 64080, 64620]).
horario('1', [61200, 61740, 62040, 62340, 62820]).
horario('11', [65220, 65700, 65940]).
horario('8', [70500,71000,71500]).
horario('5', [75000,75500,76000]).

%lista_motoristas_nworkblocks('12',[('276',2),('5188',3),('16690',2),('18107',6)]).
%lista_motoristas_nworkblocks('VeicDutT01',[('276',1),('5188',1)]).
%lista_motoristas_nworkblocks('VeicDutT02',[('276',1),('5188',2),('16690',1)]).


% Vehicle duties passam a ter este formato
% vehicleduty(ID,Data,ListaWorkBlocks)
vehicleduty('12','1/2/2000',['12','211','212','213','214','215','216','217','218','219','220','221','222']).
vehicleduty('13','1/2/2000',['223','224']).


workblock('12',['459'],34080,37620).
workblock('211',['31','63'],37620,41220).
workblock('212',['33','65'],41220,44820).
workblock('213',['35','67'],44820,48420).
workblock('214',['37','69'],48420,52020).
workblock('215',['39','71'],52020,55620).
workblock('216',['41','73'],55620,59220).
workblock('217',['43','75'],59220,62820).
workblock('218',['45','77'],62820,66420).
workblock('219',['48','82'],66420,70020).
workblock('220',['52','86'],70020,73620).
workblock('221',['56','432'],73620,77220).
workblock('222',['460'],77220,77340).

workblock('223',['56','432'],34000,34195).
workblock('224',['460'],34195,34300).

% Dps do tiago mudar a valida��o para n explodir sem prefs isto deixa de
% ser necess�rio
pref_horario('5188',28800,64800 ). %das 8:00h �s 18:00h
pref_horario('276',36000,72000 ).  %das 10:00h �s 20:00h
pref_horario('16690',0,36000).     %das 00:00h �s 10:00h
pref_horario('18107',0,86400). %das 00:00h �s 24:00h



%ASSERTS INICIAIS
%Utilizados no genetico
:-asserta(percentagem_melhor_manter(0.25)).
:-asserta(duracao_maxima(10)).
:-asserta(avaliacaoexp(1)).
:-asserta(estabiliz(4)).
:-asserta(selected_vehicle_duty('12')).
:-asserta(max_delay_time(0)). %Tempo máximo de atraso
:-asserta(first_meal_start_time(39600)). %Hora de inicio da primeira refeicao
:-asserta(first_meal_end_time(54000)). %Hora de fim da primeira refeicao
:-asserta(second_meal_start_time(64800)). %Hora de inicio da segunda refeicao
:-asserta(second_meal_end_time(79200)). %Hora de fim da primeira refeicao
:-asserta(nightshift_start_time(72000)). %Hora de inicio do periodo noturno
:-asserta(nightshift_end_time(28800)). %Hora de fim do periodo noturno
:-asserta(max_day_overtime(0)). % duracao maxima de trabalho extra durante o dia
:-asserta(max_night_overtime(0)). % duracao maxima de trabalho extra durante a noite
:-asserta(max_consecutive_hours(14400,10)). % numero maximo de horas consecutivas que o motorista pode fazer em segundos
:-asserta(max_work_hours(28800,10)). % numero maximo de horas que o motorista pode fazer no total em segundos
:-asserta(rest_hour(3600,10)). %duracao da pausa apos um bloco de 4 horas e respetiva penalizacao aquando da nao cumpricao da mesma
:-asserta(max_time_during_meal(10800)).% numero maximo que o motorista pode trabalhar durante periodo de refeicao
:-asserta(meal_break_time(3600,8)). % duracao da pausa de refeicao e respetiva penalizacao aquando da nao cumpricao da mesma
:-asserta(pref_horario_pen(1)). % penalizacao da soft constraint da preferencia de horario do motorista
:-asserta(resultado(_)).
:-dynamic t/3.


% NOVO ALGORITMO (SPRINT D)
%Facts
:-dynamic logErros/1.
:-dynamic rangevd/3.
:-dynamic t/5.
:-dynamic driver/1.
:-dynamic horariomotorista/5.

percentagem_margem_carga(0.2). %N�mero em percentagem, define qual a margem extra que as horas de trabalho dos condutores devem ter sobre as horas de trabalho necess�rios de vehicleduties

driver('2').
driver('1').
driver('276').

% horariomotorista(IdMotorista,Hcome�aTrabalhar,HacabaTrabalhar,DuracaoTotalCondu�ao,ListaDuracaoBlocosTrabalho)
horariomotorista('2',25200,61200,28800, [21600,7200]).
horariomotorista('276',25200,61200,28800, [21600,7200]).
horariomotorista('1',25200,61200,28800, [21600,7200]).

t(25200,50400,100,100,'2').

%afetarDrivers_data('1/2/2000'). %valor default



%Adiciona um elemento ao log de erros
gerarLogErros(Msg):-
    (logErros(L),!;L = []),
    append(L,[Msg],ResLogs),
     (retractall(logErros(_));true),!,asserta(logErros(ResLogs)).

%Chamada inicial ao algoritmo
afetarDrivers:-
    (retractall(list_of_facts_t(_)),!;true),
    (retractall(lista_motoristas_nworkblocks(_,_)),!;true),
    %realiza retracts necess�rios aqui
    
    inicializar_afetarDrivers,
    verificacao_carga,
    ordena(Ds,Vs), %% Ds é a lista de factos t() (blocos de trabalho de motoristas) ordenada,  Vs é a lista dos veicleDuties ordenada  tudo por ordem crescente de tempo de começo
    asserta(list_of_facts_t(Ds)), % asserted the list of facts t() becouse it will be easier to use it in recursive funtions as i will delete
    constroi_factos_motoristas_nworkblocks_inicial(Vs),
    %distribuir_blocos_hora_por_vehicle_duty(),
    %chamar_algoritmo_genetico(), %para todos os vehicle duties, vai ser preciso guardar o facto antes de ser removido no fim de cada chamada
    %gerar_driver_duties(),
    %ajustar_driver_duties(),
    !.

inicializar_afetarDrivers:-
    write('Dia: '),read(D),
    write('M�s: '),read(M),
    write('Ano: '),read(A),
    term_to_atom(D/M/A,R),
    (retractall(afetarDrivers_data(_)),!;true), asserta(afetarDrivers_data(R)),
    (retractall(logErros(_)),!;true).

%Elimina todos os factos correspondentes aos elementos da lista
deleteFacts([]):-!.
deleteFacts([X|Y]):-
    (retractall(X),!;true),
    deleteFacts(Y).

%Verifica a carga que cada motorista deve cumprir hoje
verificacao_carga:-
    gerar_range_vehicleduties,
   
    sum_horas_vehicleduties(SumVDs),
     %write('Somei vehicle duties '),write(SumVDs),
    sum_horas_motoristas(SumMots),
   % write('Somei motoristas '),write(SumMots),
    percentagem_margem_carga(Per),
    %Se h� mais horas disponiveis por motorista que horas de trabalho
    ((SumMarg is (SumVDs + SumVDs*Per),DifSums is SumMots-SumMarg,DifSums>=0,!,gerar_blocos_horas_trabalho(SumMarg),write(' Mais mots que trabalho. '))
    %Sen�o:
    ;gerarLogErros('� possivel que n�o existam motoristas com horas de trabalho suficientes para satisfazer a carga de trabalho necess�ria. M�ltiplas viola��es de hard constraints devem ser esperadas'),gerar_blocos_horas_trabalho(SumMots),write('menos mots que trabalho')).


ordena(Ds,Vs):-ordenaDrivers(Ds), ordenaVehicleDuties(Vs),!.

ordenaDrivers(R):-findall((A,B,C,D,E),t(A,B,C,D,E),L), predsort(compare_bloco_horas_mot_by_first,L, R), writeln(R).

ordenaVehicleDuties(R):-findall((D,F,A),rangevd(D,F,A),L), predsort(compare_range_vd_by_second,L, R), writeln(R).


compare_bloco_horas_mot_by_first(<, (A,_,_,_,_), (C,_,_,F,_)) :-
    ( F ==0; % se o elemento na posicao F é 0 quero metelo mais para a direita possivel da lista visto que em principio nao irá trabalhar hoje
      A @< C; %se A < C entao o primeiro arguemnto é menor e fica antes do 2º arg
      A == C).

compare_bloco_horas_mot_by_first(=, (A), (C)) :-
    A == C.

    compare_bloco_horas_mot_by_first(>, (A), (C)) :-
  ( A @> C;
   A == C ).

  
compare_range_vd_by_second(<, (_,A), (_,B)) :-
    ( A @< B; %se A < B entao o primeiro arguemnto é menor e fica antes do 2º arg
      A == B).


compare_range_vd_by_second(=, (_,A), (_,B)) :-
    A == B.

   compare_range_vd_by_second(>, (_,A), (_,B)) :-
  ( A @> B;
    A == B ).

constroi_factos_motoristas_nworkblocks_inicial([]):-!.

constroi_factos_motoristas_nworkblocks_inicial([(VdId,_,_)|T2]):-

 vehicleduty(VdId,_,L),verificaIntervaloMaiorWorkBlock(L,0,DuracaoMaiorWorkBlockVd),
obtemTempoDeInicioPrimeiroWorkBlock(L,TinicialVd),
 seleciona_mots_mais_adequados(VdId,TinicialVd,DuracaoMaiorWorkBlockVd,BestBlock),
  length(L, NumWbInVd), afetaPrimeirosWorkBlocks(VdId,NumWbInVd,DuracaoMaiorWorkBlockVd,BestBlock),
  %constroi_factos_motoristas_nworkblocks_posterior(VdId,DuracaoMaiorWorkBlockVd),
  calcNumWorkBlocksPorAfetar(VdId,NwbFalta), writeln('PXXPXPXPXPX lalalalalala = '),write(NwbFalta);
  constroi_factos_motoristas_nworkblocks_inicial(T2).
                                                                 


calcNumWorkBlocksPorAfetar(VdId,Falta):- writeln('aki KARALHO'), vehicleduty(VdId,_,L),length(L,Tamanho),writeln('aki KARALHO1'),lista_motoristas_nworkblocks(VdId,L),writeln('aki KARALHO2'), calcNumWorkBlocksPorAfetar1(Tamanho,0,L,Result), writeln('aki KARALHO3').

calcNumWorkBlocksPorAfetar1(T,A,[],R):- R is T-A.
calcNumWorkBlocksPorAfetar1(T,A[(D,N)|Tail],R):- write('ddddddddddddddddddddddddddddd'), A1 is A + N, calcNumWorkBlocksPorAfetar1(T,A1,Tail,R).

%constroi_factos_motoristas_nworkblocks_posterior(VdId,DuracaoMaiorWorkBlockVd):-
 %lista_motoristas_nworkblocks((VdId,[(D,N)|T])),(retractall(lista_motoristas_nworkblocks(VdId,_)),!;true),
 %afetaRestantesWb((VdId,[(D,N)|T]),DuracaoMaiorWorkBlockVd).
        
%afetaRestantesWb((VdId,[],DuracaoMaiorWorkBlockVd):-

%afetaRestantesWb((VdId,[(D,N)|T],DuracaoMaiorWorkBlockVd):-
%list_of_facts_t([(Ti,Tf,Dur,Tt,Driver)|T]), Div is div(D, DuracaoMaiorWorkBlockVd), RoundedDiv is round(Div),
%((RoundedDiv>0,
 %((D==Driver, N1 is N+RoundedDiv, asserta(lista_motoristas_nworkblocks(VdId,[(Driver,N1)|T])),deleteBlockFromList((Ti,Tf,Dur,Tt,Driver),[(Ti,Tf,Dur,Tt,Driver)|T]) );afetaRestantesWb((VdId,T,DuracaoMaiorWorkBlockVd))).

afetaPrimeirosWorkBlocks(VdId,NumWbInVd,DuracaoMaiorWorkBlockVd,(_,_,D,_,Driver)):-
Div is div(D, DuracaoMaiorWorkBlockVd), RoundedDiv is round(Div), ((RoundedDiv>NumWbInVd, RNum is NumWbInVd );RNum is RoundedDiv), asserta(lista_motoristas_nworkblocks(VdId,[(Driver,RNum)])),!.



seleciona_mots_mais_adequados(VdId,TinicialVd,DuracaoMaiorWorkBlockVd,BestBlock):- list_of_facts_t(ListT),seleciona_mots_mais_adequados1(TinicialVd,DuracaoMaiorWorkBlockVd,86400,ListT,BestBlock).

seleciona_mots_mais_adequados1(TinicialVd,DuracaoMaiorWorkBlockVd,A,[(Ti,Tf,Dur,Tt,Driver)|T],BestBlock):- obtainBiggestBlockOfFirstDriverInList((Ti,Tf,Dur,Tt,Driver),T,BestBlock), deleteBlockFromList(BestBlock,[(Ti,Tf,Dur,Tt,Driver)|T]).




obtainBiggestBlockOfFirstDriverInList(R,[],R):- !.%, verifica_atualiza_drivers_folga(R).
obtainBiggestBlockOfFirstDriverInList((Ti,Tf,Dur,Tt,Driver),[(Ti1,Tf1,Dur1,Tt1,Driver1)|T],Bb):- writeln('entrou'), 
((Driver == Driver1, BlockDif is Dur-Dur1, ((BlockDif<0, obtainBiggestBlockOfFirstDriverInList((Ti1,Tf1,Dur1,Tt1,Driver1),T,Bb));obtainBiggestBlockOfFirstDriverInList((Ti,Tf,Dur,Tt,Driver),T,Bb));obtainBiggestBlockOfFirstDriverInList((Ti,Tf,Dur,Tt,Driver),T,Bb))).

verificaIntervaloMaiorWorkBlock([],Acomul,Acomul):- !.
verificaIntervaloMaiorWorkBlock([H|ListWb],Acomul,R):-writeln('entrou '), workblock(H,_,Ti,Tf), write('verificar wb --'), write(H),write(' '), write(Ti),write(' '), write(Tf),
                                                  Duracao is Tf-Ti, ((Acomul<Duracao, Acomul1 is Duracao);Acomul1 is Acomul), verificaIntervaloMaiorWorkBlock(ListWb,Acomul1,R),!.

obtemTempoDeInicioPrimeiroWorkBlock([H|T],R):-write('kekekek= '),workblock(H,_,Ti,Tf),write(' DAS '), R is Ti, write(R).


%verifica_atualiza_drivers_folga((Ti1,Tf1,Dur1,0,Driver1)):- list_of_facts_t(ListT), verifica_atualiza_drivers_folga1((Ti1,Tf1,Dur1,0,Driver1),ListT).

%verifica_atualiza_drivers_folga1((Ti1,Tf1,Dur1,0,Driver1),[(Ti,Tf,Dur,T,Driver)|T]):- ((Ti1==Ti, Tf1==Tf,Dur1==Dur,Driver1==Driver, T), ).

deleteBlockFromList(BestBlock,List):- delete(List, BestBlock, R), (retractall(list_of_facts_t(_)),!;true), asserta(list_of_facts_t(R)).

% Gera factos do tipo rangevd(vehicledutyID,horaInicio,horaFim)
% referidos no forum
gerar_range_vehicleduties:-
    afetarDrivers_data(Data),
    (retractall(rangevd(_,_,_)),!;true),
    findall((VD,Hinicio,Hfim),
           (vehicleduty(VD,Data,WkList),
            calcular_horario_workblocks(WkList,Hinicio,Hfim))
           ,RangeList),
    gerar_range_vehicleduties1(RangeList).

gerar_range_vehicleduties1([]):-!.
gerar_range_vehicleduties1([(Vd,Hi,Hf)|Y]):-
    asserta(rangevd(Vd,Hi,Hf))
    ,gerar_range_vehicleduties1(Y).


% Obtem de uma lista de workblocks, o seu horario de inicio e o seu
% horario de fim
calcular_horario_workblocks([X],Hinicio,Hfim):-workblock(X,_,Hinicio,Hfim).
calcular_horario_workblocks([X|Y],Hinicio,Hfim):-
   workblock(X,_,Hi,Hf),
   calcular_horario_workblocks(Y,Hinicio1,Hfim1),
   (Hi<Hinicio1,!,Hinicio = Hi;Hinicio = Hinicio1),
   (Hf>Hfim1,!,Hfim = Hf;Hfim = Hfim1).


% Obt�m o somatorio do tempo de trabalho necess�rio para realizar todos
% os vehicle duties
sum_horas_vehicleduties(Sum):-
    findall(T,
           (rangevd(_,Hi,Hf), T is Hf - Hi)
           ,Rlist),
    sumAllElems(Rlist,Sum).

%Retorna o somatorio de todos os elementos de uma lista de numeros
sumAllElems([],0):-!.
sumAllElems([X|Y],Sum):-
    sumAllElems(Y,Sum1),
    Sum is Sum1 + X.

% Obt�m o somatorio do tempo de trabalho disponibilizado por todos os
% motoristas
sum_horas_motoristas(Sum):-
    findall(T,
           horariomotorista(_,_,_,T,_)
           ,Rlist),
    sumAllElems(Rlist,Sum).

% Gera os factos t(Hi,Hf,TMaxtrabalho,TatualTrabalho,IdMotorista) que
% representam os blocos de horas de trabalho de cada motorista
gerar_blocos_horas_trabalho(TempoTrabalho):-
    motoristas_menor_trabalho_total(ListaMotOrd),
    add_new_drivers(ListaMotOrd,ListaTdsMots),
    (retractall(t(_,_,_,_,_)),!;true),
    gerar_blocos_horas_trabalho1(TempoTrabalho,ListaTdsMots).

gerar_blocos_horas_trabalho1(_,[]):-!.
gerar_blocos_horas_trabalho1(TempoTrabalho,[Driv|Y]):-
    horariomotorista(Driv,Hi,Hf,TotalT,ListaBlocos),
    gerar_blocos_motorista(Driv,Hi,Hf,TotalT,ListaBlocos,TempoTrabalho,Tutilizado),
    Trestante is TempoTrabalho - Tutilizado,
    gerar_blocos_horas_trabalho1(Trestante,Y).

%Condi��o paragem
gerar_blocos_motorista(Driv,Hi,Hf,_,[X],Trestante,Tutilizado):-
    !,
    ((Trestante=<0,!,TatualTrabalho is 0);(TatualTrabalho is X)),
    assert(t(Hi,Hf,X,TatualTrabalho,Driv)),
    Tutilizado is X.

gerar_blocos_motorista(Driv,Hi,Hf,TotalT,[X|Y],Trestante,Tutilizado):-
    ((X>=14400,!, sumAllElems([X|Y],Sum), Diff is Hf-Hi, Diff>Sum+3600,HblocoF is Hi + X + 3600);HblocoF is Hi + X), %Se o tempo ocupado por X for >= a 4 horas, � necess�rio adicionar algum tempo de descanso
    ((Trestante=<0,!,TatualTrabalho is 0);(TatualTrabalho is X)), %Verificar se � necess�rio alocar tempo a este driver ou se ele fica livre
    assert(t(Hi,HblocoF,X,TatualTrabalho,Driv)),
    gerar_blocos_motorista(Driv,HblocoF,Hf,TotalT,Y,Trestante,Tutilizado1),
    Tutilizado is Tutilizado1 + X.


% Obtem por ordem de menor trabalho anterior para a empresa uma lista
% com os ids dos motoristas (do menor para o maior)
%
motoristas_menor_trabalho_total(ListaMotOrd):-
    findall((Total,Driv),
           t(_,_,_,Total,Driv)
           ,Lista),
    agregarValoresId(Lista,TempList),
    sort(TempList,TempList1),
    motoristas_menor_trabalho_total1(TempList1,ListaMotOrd).


%Limpa os valores da lista, mantendo apenas os Ids dos drivers
motoristas_menor_trabalho_total1([],[]):-!.
motoristas_menor_trabalho_total1([(_,Id)|Y],ListaMotOrdLimpa):-
    motoristas_menor_trabalho_total1(Y,ListaMotOrdLimpa1),
    append([Id],ListaMotOrdLimpa1,ListaMotOrdLimpa).

% Numa lista de dupletos de formato (Id,Valor), agrega todos os
% elementos com o mesmo ID e soma o seu valor
agregarValoresId([],[]):-!.
agregarValoresId([(V,Id)|Y],ListaMotOrd):-
    agregarValoresId(Y,ListaMotOrd1),
    addValorId((V,Id),ListaMotOrd1,ListaMotOrd).

% Retorna uma nova lista ap�s somar o valor ao dupleto com Id
% correspondente, caso id correspondente n�o exista, adiciona-o ao final
% da lista
addValorId((V,Id),[],[(V,Id)]):-!.
addValorId((V,Id),[(V1,Id1)|Y],NewList):-
    ((Id == Id1,!,Value is V+V1,append([(Value,Id)],Y,NewList));
    (addValorId((V,Id),Y,NewList1),append([(V1,Id1)],NewList1,NewList))).

% Adiciona todos os drivers que n�o trabalharam anteriormente ao inicio
% da lista
add_new_drivers(ListaMotOrd,ListaTdsMots):-
    findall(Id,
            (driver(Id),\+ member(Id,ListaMotOrd))
           ,ListaDrivers),
    append(ListaDrivers,ListaMotOrd,ListaTdsMots).





































% COISAS ANTERIORES
% A partir daqui � tudo coisas de sprints anteriores, se forem realizar
% mudan�as aqui, lembrem-se de as realizar tambem no ficheiro
% planning.pl


% Astar (lembrar que s� corre direito com as coisas do reloadData (que
% n�o � suposto estarem aqui) ou todos os factos estaticos)
aStar(Hinit,Orig,Dest,Cam,CamL,Custo):-
        aStar2(Hinit,Dest,[(_,Hinit,[Orig],[])],Cam,CamL,Custo).


aStar2(Hinit,Dest,[(_,Ha,[Dest|T],Linhas)|_],Cam,CamL,Custo):-
        reverse([Dest|T],Cam),reverse(Linhas,CamL), Custo is (Ha-Hinit).

aStar2(Hinit,Dest,[(_,Ha,LA,Linhas)|Outros],Cam,CamL,Custo):-
        LA=[Act|_],
        findall((HEX,HaX,[X|LA],[Linha|Linhas]),
                (Dest\==Act,ligaCustos(Ha,Act,X,HcustoX,Linha),\+ member(X,LA), \+ member(Linha,Linhas),
                 HaX is HcustoX + Ha, estimativa(X,Dest,EstX),
                 HEX is HaX +EstX),Novos),
        append(Outros,Novos,Todos),
        sort(Todos,TodosOrd),
        aStar2(Hinit,Dest,TodosOrd,Cam,CamL,Custo).

estimativa(Nodo1,Nodo2,Estimativa):-
        velocidadeMedia(Vm),
        nodes(_,Nodo1,_,_,X1,Y1),
        nodes(_,Nodo2,_,_,X2,Y2),
        Dist is sqrt((X1-X2)^2+(Y1-Y2)^2),
        Estimativa is Dist/Vm.

velocidadeMedia(Vm):-
        findall(Vel,(linhas(_,_,_,Tempo,Dist),Vel is Dist/Tempo),Lista),
        max_list(Lista,Vm).


% Algoritmo gen�tico (est� a ser utilizado apenas o gera_populacao sem
% heuristicas para n haver problemas)
gera_genetico(VeicDuty,Duracao):-
 inicializa_gera_genetico(VeicDuty,Duracao),!,
gera_populacao1(Pop),!,
avalia_populacao(Pop,PopAv),!,
ordena_populacao(PopAv,PopOrd),
geracoes(NG),
asserta(melhor(_*100 )),
estabiliz(Establimit),!,
gera_geracao(0,NG,PopOrd,1,Establimit,0,0),!.

inicializa_gera_genetico(VeicDuty,Duracao):-
 (retract(geracoes(_));true),asserta(geracoes(100)),
 (retract(populacao(_));true),asserta(populacao(2)),
 (retract(prob_cruzamento(_));true),asserta(prob_cruzamento(75)),
 (retract(prob_mutacao(_));true),asserta(prob_mutacao(25)),
 (retract(selected_vehicle_duty(_));true),asserta(selected_vehicle_duty(VeicDuty)),
 (retract(avaliacaoexp(_));true),asserta(avaliacaoexp(1)),
 (retract(duracao_maxima(_));true),asserta(duracao_maxima(Duracao)),
 get_time(Ta),
 (retract(tempo_inicial(_));true), asserta(tempo_inicial(Ta)),!.


gera_populacao1(Pop):-
populacao(TamPop),
dimensao(NumD),
selected_vehicle_duty(Vehic),
lista_motoristas_nworkblocks(Vehic,ListaDrivers),
transformNewListDrivers(ListaDrivers,[],NewListaDrivers),
gera_populacao1(TamPop,NewListaDrivers,NumD,Pop).


dimensao(Dim):- selected_vehicle_duty(Vd),!,lista_motoristas_nworkblocks(Vd,L),
    dimensao2(L,Dim).
dimensao2([],0).
dimensao2([(_,X)|Z],Dim):- dimensao2(Z,Dim1),Dim is Dim1 + X.

transformNewListDrivers([],L,L):-!.
transformNewListDrivers([(Driver,Vezes)|L],List,R):-
addToListXTimes(Driver,Vezes,List,ResultList),
transformNewListDrivers(L,ResultList, R).


addToListXTimes(_,0,L,L):-!.
addToListXTimes(Driver,Vezes,List,Res):-
Vezes1 is Vezes-1,
append(List,[Driver],L1),
addToListXTimes(Driver,Vezes1,L1,Res).


gera_populacao1(0,_,_,[]):-!.
gera_populacao1(TamPop,ListaDrivers,NumD,[Ind|Resto]):-
TamPop1 is TamPop-1,
gera_populacao1(TamPop1,ListaDrivers,NumD,Resto),
gera_individuo(ListaDrivers,NumD,Ind),
not(member(Ind,Resto)).


gera_populacao1(TamPop,ListaDrivers,NumD,L):-
gera_populacao1(TamPop,ListaDrivers,NumD,L).


gera_individuo([G],1,[G]):-!.
gera_individuo(ListaDrivers,NumD,[G|Resto]):-
NumTemp is NumD + 1, % para usar com random
random(1,NumTemp,N),
retira(N,ListaDrivers,G,NovaLista),
NumT1 is NumD-1,
gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto):-!.
retira(N,[G1|Resto],G,[G1|Resto1]):- N1 is N-1,
retira(N1,Resto,G,Resto1).


avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
criar_agenda_temporal(Ind,ATemp),

avalia(ATemp,V),
avalia_populacao(Resto,Resto1).

avalia(Agenda,V):-avalia2(Agenda,[],V),!.

avalia2([ ],_,0).

avalia2([(Tinicio,Tfim,Driver)|Resto],ListaDriversVistos,V):-
%se for a primeira vez que estiver a avaliar um driver, ve as restricoes onde e necessario todos os blocos da agenda do mesmo
%e adiciona a lista de drivers vistos para quando for ver outro bloco do condutor nao repetir a verificacao destas restricoes
(( \+ member(Driver,ListaDriversVistos),!,append(ListaDriversVistos,[Driver],ListaDriversVistos2),!,findall((Ti,Tf,Driver),member((Ti,Tf,Driver),Resto),ListaCondutor)
,check_restriction_max_hours(Tinicio,Tfim,ListaCondutor,VT3),check_restriction_meal(Tinicio,Tfim,ListaCondutor,VT4),check_pref_horario(Tinicio,Tfim,Driver,ListaCondutor,VT5)); append(ListaDriversVistos,[],ListaDriversVistos2), VT3 is 0 ,VT4 is 0 ,VT5 is 0),

check_restriction_consecutive_hours(Tinicio,Tfim,VT2),
check_restriction_rest_after_4hours(Tinicio,Tfim,Driver,Resto,VT),
avalia2(Resto,ListaDriversVistos2,VResto),

V is VT+VT2+VT3+VT4+VT5+VResto .

%predicado que verifica se os blocos da agenda estao dentro da preferencia de horario do motorista,caso nao esteja e uma violacao e e calculada a penalizacao
check_pref_horario(Tinicio,Tfim,Driver,ListaCondutor,VT5):-pref_horario(Driver,T1,T2),pref_horario_pen(Pen),!,check_pref_horario2(Tinicio,Tfim,ListaCondutor,T1,T2,Total), VT5 is Total*Pen.

check_pref_horario2(Tinicio,Tfim,[],T1,T2,Total):-(((Tinicio>T2;T1>Tfim),!,Total is Tfim-Tinicio);((Tinicio<T1,T1=<Tfim,Tfim=<T2,!, Total is T1-Tinicio) ; (Tinicio >=T1,T2>Tinicio,T2=<Tfim,!, Total is Tfim-T2));Total is 0).
check_pref_horario2(Tinicio,Tfim,[(Tinicio2,Tfim2,_)|Resto],T1,T2,Total):-check_pref_horario2(Tinicio,Tfim,Resto,T1,T2,Total2),(((Tinicio2>T2; T1>Tfim2),Total is Total2+(Tfim2-Tinicio2));((Tinicio2<T1,T1=<Tfim2,Tfim2=<T2, Total is Total2+(T1-Tinicio2)) ; (Tinicio2 >=T1,T2>Tinicio2,T2=<Tfim2, Total is Total2+(Tfim2-T2)));Total is Total2+0).


%predicado que vai verificar se na agenda do motorista, nos periodos de refeicoes tem 1 hora de pausa
check_restriction_meal(Tinicio,Tfim,ListaCondutor,VT4):-!,check_lunch_time(Tinicio,Tfim,ListaCondutor,VT1),check_dinner_time(Tinicio,Tfim,ListaCondutor,VT2),!, VT4 is VT1+VT2.

% verificar se o condutor durante o periodo do almoco possui um hora para a refeicao, caso não tenha é uma violacao de 3600 segundos
check_lunch_time(Tinicio,Tfim,ListaCondutor,VT1):-meal_break_time(H,Pen),first_meal_start_time(T1),first_meal_end_time(T2),max_time_during_meal(M),check_lunch_time2(Tinicio,Tfim,ListaCondutor,T1,T2,Total), ((Total >M, VT1 is H*Pen);VT1 is 0).
% predicado auxiliar que vai calcular a intersecao entre o periodo de almoco e a agenda do motorista
check_lunch_time2(Tinicio,Tfim,[],T1,T2,Total):-(((Tinicio>T2;T1>Tfim),!,Total is 0); (Ta is max(T1,Tinicio),Tb is min(T2,Tfim), Total is Tb-Ta)).
check_lunch_time2(Tinicio,Tfim,[(Tinicio2,Tfim2,_)|Resto],T1,T2,Total):-check_lunch_time2(Tinicio,Tfim,Resto,T1,T2,Total2),(((Tinicio2>T2;T1>Tfim2),Total is Total2+0); (Ta is max(T1,Tinicio2),Tb is min(T2,Tfim2), Total is Total2+(Tb-Ta))).

% verificar se o condutor durante o periodo do jantar possui um hora para a refeicao, caso não tenha é uma violacao de 3600 segundos
check_dinner_time(Tinicio,Tfim,ListaCondutor,VT1):-meal_break_time(H,Pen),second_meal_start_time(T1),second_meal_end_time(T2),max_time_during_meal(M),check_dinner_time2(Tinicio,Tfim,ListaCondutor,T1,T2,Total), ((Total >M, VT1 is H*Pen);VT1 is 0).
% predicado auxiliar que vai calcular a intersecao entre o periodo de jantar e a agenda do motorista
check_dinner_time2(Tinicio,Tfim,[],T1,T2,Total):-!,(((Tinicio>T2;T1>Tfim),Total is 0); (Ta is max(T1,Tinicio),Tb is min(T2,Tfim), Total is Tb-Ta)).
check_dinner_time2(Tinicio,Tfim,[(Tinicio2,Tfim2,_)|Resto],T1,T2,Total):-check_dinner_time2(Tinicio,Tfim,Resto,T1,T2,Total2),(((Tinicio2>T2;T1>Tfim2),Total is Total2+0); (Ta is max(T1,Tinicio2),Tb is min(T2,Tfim2), Total is Total2+(Tb-Ta))).

% predicado que verifica se um bloco da agenda do motorista passa o limite de horas consecutivas possiveis, se passou entao e uma violacao e calcular a penalizacao
check_restriction_consecutive_hours(Tinicio,Tfim,VT):-
max_consecutive_hours(M,Pen), Ttotal is Tfim-Tinicio,((Ttotal=<M,!,VT is 0);(VT is (Ttotal-M)*Pen)).

% predicado que verifica se o somatorios dos blocos da agenda do motorista passa o limite de horas diarias possiveis, se passou entao e uma violacao e calcular a penalizacao
check_restriction_max_hours(Tinicio,Tfim,ListaCondutor,VT):-
max_work_hours(M,Pen),get_total_workhours(Tinicio,Tfim,ListaCondutor,Total),((Total=<M,!,VT is 0);(VT is (Total-M)*Pen)).
%predicado auxiliar que obtem as horas de trabalho total do motorista
get_total_workhours(Tinicio,Tfim,[],Total):-!,Total is Tfim-Tinicio.
get_total_workhours(Tinicio,Tfim,[(Tinicio2,Tfim2,_)|Resto],Total):-get_total_workhours(Tinicio,Tfim,Resto,Total2),Total is Total2 + (Tfim2-Tinicio2).

%predicado que verifica se o bloco atual for de 4 horas, ve se a diferenca entre esse e o proximo na agenda e igual ou superior ao tempo de descanso (1 hora), se viola esta condicao e calculada a penalizacao
check_restriction_rest_after_4hours(Tinicio,Tfim,Driver,Resto,VT):-
rest_hour(M,Pen),max_consecutive_hours(M2,_),Diff is (Tfim-Tinicio), (Diff=M2 ,!,findall((Ti,Tf,Driver),member((Ti,Tf,Driver),Resto),ListaCondutor),check_rest_time(Tinicio,Tfim,ListaCondutor,Total),((Total >= M,!,VT is 0);(VT is M*Pen))); VT is 0.
%predicado auxiliar que calcula a diferenca entre o fim do bloco atual e o inicio do proximo
check_rest_time(_,_,[],Total):-Total is 9999. %se for o ultimo bloco do condutor, atribuimos um valor maior que 3600
check_rest_time(_,Tfim,[(Tinicio2,_,_)|_],Total):-Total is Tinicio2-Tfim.

%predicado que cria a agenda temporal para facilitar a avaliacao
criar_agenda_temporal(Ind,Agenda):-selected_vehicle_duty(D),
vehicleduty(D,ListaWorkblocks),
criar_agenda_temporal2(Ind, ListaWorkblocks,-1),
preencher_agenda(Agenda).

preencher_agenda(Agenda):-!,findall((Ti,Tf,D),t(Ti,Tf,D),Agenda),retractall(t(_,_,_)).
%predicado auxiliar que ao criar os periodos do motorista, condensa os consecutivos
criar_agenda_temporal2([H1],[Hw],Ti):-Ti = -1,(workblock(Hw,_,Tinicio,Tfinal),assertz(t(Tinicio,Tfinal,H1))),!; (workblock(Hw,_,_,Tfinal),assertz(t(Ti,Tfinal,H1))),!.

criar_agenda_temporal2([H1|[H2|T1]],[Hw|Tw],Ti):-
  Ti = -1 ,( (H1 = H2 ,
workblock(Hw,_,Tinicio,_),!,criar_agenda_temporal2([H2|T1],Tw,Tinicio));(workblock(Hw,_,Tinicio,Tfinal),assertz(t(Tinicio,Tfinal,H1)),!, criar_agenda_temporal2([H2|T1],Tw,-1)) )  ;
( (H1 = H2 ,!,criar_agenda_temporal2([H2|T1],Tw,Ti)) ;(workblock(Hw,_,_,Tfinal),assertz(t(Ti,Tfinal,H1)),!,criar_agenda_temporal2([H2|T1],Tw,-1)) ).


ordena_populacao(PopAv,PopAvOrd):-
bsort(PopAv,PopAvOrd).


bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
bsort(Xs,Zs),
btroca([X|Zs],Ys).


btroca([X],[X]):-!.
btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
VX>VY,!,
btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).



gera_geracao(_,_,Pop,E,E,_,_):-!,
retract(resultado(_)),asserta(resultado(Pop)).

gera_geracao(_,_,Pop,_,_,_,1):-!,
retract(resultado(_)),asserta(resultado(Pop)).

gera_geracao(_,_,Pop,_,_,1,_):-!,
retract(resultado(_)),asserta(resultado(Pop)).

gera_geracao(G,G,Pop,_,_,_,_):-!,
retract(resultado(_)),asserta(resultado(Pop)).



gera_geracao(N,G,Pop,Estab,Establimit,_,_):-
verifica_tempo(Limitreach1),
verifica_avaliacao_especifica(Pop,Avalespecifica1),
cruzamento(Pop,NPop1),
mutacao(NPop1,NPop),
avalia_populacao(NPop,NPopAv),
ordena_populacao(NPopAv,NPopOrd),
append(Pop,NPopOrd,Epop),
sort(Epop,Espop),
ordena_populacao(Espop,Rpop),
populacao(Num),
percentagem_melhor_manter(Pm),
P1 is Num * Pm,
(P1<1 ->P is 1; P is round(P1)),
obterListas(Rpop,P,Lm,Lp),
flatten([Lm|Lp], Lfx),
N1 is N+1,
verifica_estabilizacao(Pop,Estab,Lfx,Restabilizacao),
gera_geracao(N1,G,Lfx,Restabilizacao,Establimit,Limitreach1,Avalespecifica1).


obterListas(Pop,P,Listadosmelhores,Listadospiores):- obterListaMelhores(Pop,P,Listadosmelhores), obterListaPiores(Pop,P,Listadospiores).

obterListaMelhores(_,0,[]).
obterListaMelhores([M|Pop],P,[M|Pop1]):-P>=0, P1 is P-1, obterListaMelhores(Pop,P1,Pop1).


obterListaPiores(L,0,R):-randomizeListaPiores(L,P), trataListaPioresRandomized(P,T),devolveListaSemRandom(T,R).
obterListaPiores([_|Pop],P,R):- P1 is P-1 , obterListaPiores(Pop,P1,R).

randomizeListaPiores([],[]).
randomizeListaPiores([H*Vp|T],[H*Vp*Vprand|T1]):- random(0.000001,0.999999,Random),Vprand is round(Vp*Random),randomizeListaPiores(T,T1).

trataListaPioresRandomized(T,P):-ordena_populacao(T,Tord),obterNumeroDeElementosConsiderarListaPiores(R), selecionaParteDaLista(Tord,R,P).

obterNumeroDeElementosConsiderarListaPiores(R):-populacao(Num),percentagem_melhor_manter(Pm),T is round(Num*Pm), R is Num-T.

selecionaParteDaLista(_,0,[]).
selecionaParteDaLista([H|T],R,[H|T1]):- R1 is R-1, selecionaParteDaLista(T,R1,T1).

devolveListaSemRandom([],[]).
devolveListaSemRandom([H*Vp*_|T],[H*Vp|T1]):-
	devolveListaSemRandom(T,T1).



verifica_tempo(Limitreach):-duracao_maxima(D), tempo_inicial(I), get_time(Tf), R is D+I, (R>Tf ->Limitreach is 0;Limitreach is 1).

verifica_avaliacao_especifica([_*VX|_],Avalespecifica):- avaliacaoexp(Aval), (VX =< Aval -> Avalespecifica is 1;Avalespecifica is 0  ).


verifica_estabilizacao(Pop,Estab,Novapop,R):- (Pop = Novapop -> R is Estab+1; R is 1).


gerar_pontos_cruzamento(P1,P2):- gerar_pontos_cruzamento1(P1,P2).
gerar_pontos_cruzamento1(P1,P2):-
dimensao(N),
NTemp is N+1,
random(1,NTemp,P11),
random(1,NTemp,P21),
P11\==P21,!,
((P11<P21,!,P1=P11,P2=P21);P1=P21,P2=P11).



gerar_pontos_cruzamento1(P1,P2):-
gerar_pontos_cruzamento1(P1,P2).


cruzamento(L,R):- random_permutation(L,Lrp), cruzamento2(Lrp,R).

cruzamento2([ ],[ ]).
cruzamento2([Ind*_],[Ind]).



cruzamento2([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
gerar_pontos_cruzamento(P1,P2),
prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
((Pc =< Pcruz,!,
cruzar(Ind1,Ind2,P1,P2,NInd1),
cruzar(Ind2,Ind1,P1,P2,NInd2))
;
(NInd1=Ind1,NInd2=Ind2)),
cruzamento2(Resto,Resto1).


%  PreencheH
preencheh([ ],[ ]).

preencheh([_|R1],[h|R2]):-
 preencheh(R1,R2).


 %  Sublista
sublista(L1,I1,I2,L):-I1 < I2,!,
 sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!, preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,N3 is N2 - 1,
 sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-N3 is N1 - 1,
 N4 is N2 - 1,
 sublista1(R1,N3,N4,R2).


%  Rotate_Right
rotate_right(L,K,L1):- dimensao(N),
 T is N - K,
 rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):- N1 is N - 1,
 append(R,[X],R1),
 rr(N1,R1,R2).



elimina([],_,[]):-!.
elimina([X|R1],L,[X|R2]):- not(member(X,L)),!,
elimina(R1,L,R2).
elimina([_|R1],L,R2):-
elimina(R1,L,R2).

% Valida se h� espa�o para o elemento ser colocado na lista, true
% se houver
validarElem(X,L):-
    obtainNumberOfMaxDriverOccurs(X,N),count(X,L,N1),!,N>N1.

%  Obtem o n�mero m�ximo de ocurrencias do condutor possiveis para o
% vehicle duty atual
obtainNumberOfMaxDriverOccurs(X,N):-
    selected_vehicle_duty(Vd),
    lista_motoristas_nworkblocks(Vd,L),
    nth0(_,L,(X,N)).

%  Counts the amount of ocurrences N of element X in list
count(_,[],0):-!.
count(X,[A|B],N):-
    count(X,B,N1),
    ((A==X,N is N1+1);(N is N1)).


%  Insere

%  Condi��o de paragem, se chegarmos ao elemento do ponto de corte 1
insere(P1,_,L,N,L):-
 dimensao(T),
 N>T,N1 is N mod T,N1==P1,!.

insere(P1,[X|R],L,N,L2):-
 dimensao(T),
 ((N>T,!,N1 is N mod T);N1 is N),
 ((validarElem(X,L),!,
   insere1(X,N1,L,L1),
   N2 is N + 1);
 (L1 = L,N2 is N)),
 insere(P1,R,L1,N2,L2).

insere1(X,1,L,[X|L]):-!.

insere1(X,N,[Y|L],[Y|L1]):-
 N1 is N-1,
 insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
 sublista(Ind1,P1,P2,Sub1),
 dimensao(NumT),
 R is NumT-P2,
 rotate_right(Ind2,R,Sub2),
 P3 is P2 + 1,
 insere(P1,Sub2,Sub1,P3,NInd1),
 eliminah(NInd1,NInd11).


eliminah([],[]).
eliminah([h|R1],R2):-!,
eliminah(R1,R2).
eliminah([X|R1],[X|R2]):-
eliminah(R1,R2).


mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
prob_mutacao(Pmut),
random(0.0,1.0,Pm),
((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
mutacao(Rest,Rest1).
mutacao1(Ind,NInd):-
gerar_pontos_cruzamento(P1,P2),
mutacao22(Ind,P1,P2,NInd).
mutacao22([G1|Ind],1,P2,[G2|NInd]):-
!, P21 is P2-1,
mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
P11 is P1-1, P21 is P2-1,
mutacao22(Ind,P11,P21,NInd).
mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
P1 is P-1,
mutacao23(G1,P1,Ind,G2,NInd).
