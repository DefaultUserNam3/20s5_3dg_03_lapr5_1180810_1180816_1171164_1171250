export const environment = {
	production: true,
	MDRURL: 'https://lapr5-20s5-mdr.herokuapp.com',

	PlanningURL: 'https://uvm039.dei.isep.ipp.pt',
	MDVURL: 'https://mdv-g39-01.azurewebsites.net'
};
