/****** Script for SelectTopNRows command from SSMS  ******/
DELETE FROM DBO.Roles;
DELETE FROM DBO.Users;
DELETE FROM DBO.DriverDuties;
DELETE FROM DBO.Drivers;
DELETE FROM DBO.PassingTimes;
DELETE FROM DBO.VehicleDuties;
DELETE FROM DBO.Trips;
DELETE FROM DBO.TripWorkBlock;
DELETE FROM DBO.Vehicles;
DELETE FROM DBO.WorkBlocks;
